import json

from coalib.misc.Shell import run_shell_command
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django_common.http import JsonResponse
from coala_web.settings import BASE_DIR
from utils.check_for_valid_bears import check_for_valid_bears

from brake.decorators import ratelimit

# FIXME: Replace with
# from coala_utils.FileUtils import create_tempfile
# when coala is upgraded to 0.10
import tempfile, os
def create_tempfile(suffix="", prefix="tmp", dir=None):
    """
    Creates a temporary file with a closed stream
    The user is expected to clean up after use.
    :return: filepath of a temporary file.
    """
    temporary = tempfile.mkstemp(suffix=suffix, prefix=prefix, dir=dir)
    os.close(temporary[0])
    return temporary[1]


@ratelimit(block=True, rate='20/m')
@csrf_exempt
@require_http_methods(["GET", "POST"])
def editor(request):
    request = json.loads(request.body.decode("utf-8"))
    file_query = request["file_data"]
    bear_query = request["bears"]

    bear_query = "".join(bear_query.split())
    bears = bear_query.split(',')
    if not check_for_valid_bears(bears):
        return HttpResponse('{ "status": "error", "msg": "Bears not valid!"}')

    filename = create_tempfile(prefix="tmp", dir=BASE_DIR + '/tmp/')

    with open(filename, 'w') as pointer:
        pointer.write(file_query)
        pointer.close()

    cmd = "coala --json --files={file} --bears={bears} -I".format(bears=bear_query, file=filename)

    shell_output, shell_err = run_shell_command(cmd)

    os.remove(filename)

    return JsonResponse(json.loads(shell_output))
